#!/bin/bash


# USAGE: bash run_kmafinder_for_multiple.sh

# Create a directory for all your fastq files and softlink the fastq files into that directory
# Set the following three variables

# 1. full path to directory containing the multifasta sequence database
MyKMAfinder_DB=<MyKMAfinder_DB> 
# 2. full path to directory containing the input fastq files
MyWorkDir=<MyWorkDir>
# 3. provide the name of the fasta database file with file extension e.g., staph_biofilm_genes.fasta
fasta_db_filename=<fasta_db_filename>

output_dir=results

# Copy raw files to a temp dir if files in input dir are symlinks
mkdir ${MyWorkDir}/analysis_dir
for file in $(ls ${MyWorkDir}/*.gz); do 
	if [[ -L ${MyWorkDir}/${file} ]]; then 
		#file_name=$(basename $file)
	        file_path=$(readlink ${MyWorkDir}/${file})
	        echo "Copying $file_path to ${MyWorkDir}/analysis_dir"
	        cp $file_path ${MyWorkDir}/analysis_dir
	else
		cp ${MyWorkDir}/${file} ${MyWorkDir}/analysis_dir
	fi
done

MyWorkDir=${MyWorkDir}/analysis_dir


cd $MyWorkDir
num_files=$(ls ${MyWorkDir}/*_1*.gz | wc -l)
i=1
for file in $(ls ${MyWorkDir}/*_1*.gz); 
do 
	filename=$(basename $file) && id=${filename%_*.gz};
	echo "Running mykmafinder for $id ($i out of $num_files...)"
	((i=i+1))
	docker run --rm -it -v $MyKMAfinder_DB:/database -v $MyWorkDir:/workdir \
		mykmafinder \
			-i ${id}*.gz \
			-o /workdir/${output_dir}/${id} \
			-dbn index_db \
			-dbf /database/${fasta_db_filename} \
			-ID 90 \
			-ks 21
	# rename results table file
	mv ${MyWorkDir}/results/${id}/results.res ${MyWorkDir}/results/${id}/results.tsv
	# Uncomment next line to remove unneeded files. Can use '-sm' flag with the kmafinder run instead
	rm -rf ${MyWorkDir}/results/${id}/*.frag.gz ${MyWorkDir}/results/${id}/*.aln
done

if [[ $? == 0 ]]
then
	# make sure 'combine_kmafinder_results.py' is available in your path
	echo "Combining results output..."
	combine_kmafinder_results.py ${MyWorkDir}/${output_dir}
else
	echo "Something went wrong"
fi

# Remove temporary files
rm -rf ${MyWorkDir}/*.gz $MyKMAfinder_DB/index_db*

# move the analysis results to the parent directory of the fastqs directory
mv ${MyWorkDir}/${output_dir}/ ${MyWorkDir}/../../

