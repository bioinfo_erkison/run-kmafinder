# Run kmafinder

## Description
These are two scripts used to run the kmafinder tool to predict matches for multiple fastq reads in a custom  fasta database.


## Dependencies/installation
You would need to have the following installed on your computer
- [MyKMAfinder](https://bitbucket.org/genomicepidemiology/mykmafinder/src/master/)
- Python3 
- [Pandas](https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html)

Follow the instructions in the link above to have kmafinder installed on your computer and copy the `combine_kmafinder_results.py` script to your path. 

Open the script in a text editor to set the variables for the locations of the input read files and database files. Save the script and run `bash run_kma_finder_for_multiple.sh`

