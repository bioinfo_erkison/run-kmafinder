#!/usr/bin/python3

import os, sys, json
import pandas as pd

def get_result_dir_list(results_dir):
    dir_list = os.listdir(results_dir)
    results_dir_list = list()
    for line in dir_list:
        line = line.strip()
        if line == "" or "summary" in line or line.startswith(".") : pass
        else:
            results_dir_list.append(line)
    return results_dir_list

def parse_results(results_dir_list, results_dir):
    all_hits = dict()
    for results_sample_dir in results_dir_list:
        sample_id = results_sample_dir
        json_res_file = "{0}/{1}/data.json".format(results_dir, results_sample_dir)
        with open(json_res_file, 'r') as f:
            data = json.loads(f.read())
        sample_hits_dict = data["mykmafinder"]["results"]["species_hits"]
        for hit in sample_hits_dict:
            if all_hits.get(hit):
                all_hits[hit].append(sample_id)
            else:
                all_hits[hit] = [sample_id]
    # print(all_hits)
    
    hits_sample_presence_absence_dict = dict()
    for hit, samples_list in all_hits.items():
        hits_sample_presence_absence_dict[hit] = dict()
        for sample_id in results_dir_list:
            if sample_id in samples_list:
                hits_sample_presence_absence_dict[hit][sample_id] = "yes"
            else:
                hits_sample_presence_absence_dict[hit][sample_id] = "no"
    # print(hits_sample_presence_absence_dict)
    return hits_sample_presence_absence_dict


def write_output(hits_sample_presence_absence_dict, results_dir):
    output_df = pd.DataFrame({key: value for key, value in hits_sample_presence_absence_dict.items()})
    output_df.index.name = "Samples"
    output_df.to_csv("{}/mykma_finder_summary.csv".format(results_dir), encoding='utf-8')
    print("\nSuccess!\n\nSee combined results summary in '{}/mykma_finder_summary.csv'".format(results_dir))

                
# Usage:
# combine_kmafinder_results.py <path_to_kmafinder_output_directory>

results_dir = sys.argv[1]
results_dir_list = get_result_dir_list(results_dir)
hits_sample_presence_absence_dict = parse_results(results_dir_list, results_dir)
write_output(hits_sample_presence_absence_dict, results_dir)



